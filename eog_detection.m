function [eog_detected] = eog_detection(s, Th, fs, Comp, bp_order, bp_band) 
% [EOGIndex] = eog_detection(s, Th, fs, [Comp, bp_order, bp_band]) 
% 
% Given EOG electrodes it computes EOG detection based on thresholding the
% signal envelop. The signal is bandpassed before the envelop.
% It returns a binary wave with samples contaiminated by
% EOG. The detection is based on horizontal, vertical and radial [optional]
% components.
%
% s         -- points x channels array. Convention for EOG channels: 
%                                       1 - right eye       [for horizontal, vertical, radial component]
%                                       2 - central front   [for horizontal, vertical, radial component]
%                                       3 - left eye        [for horizontal, vertical, radial component]
%                                       4 - Occipital       [for radial component]
% Th        -- Threshold in the signal unit
% fs        -- Signal sampling frequency
% Comp      -- Components: 'VHR' (Vertical, Horizontal and Radial) or 'VH'
%              (Vertical, Horizontal)
% bp_order  -- order for the bandpass filter [4]
% bp_band   -- band for the bandpass filter [1 7]

    if nargin == 1 || nargin == 2
        error('chk:arg', 'Threshold and fs are mandatory');
    end
    
    if nargin == 3
        Comp = 'VHR';
        bp_order = 3;
        bp_band  = [1 7];
    end
    
    if nargin == 4
        bp_order = 3;
        bp_band  = [1 7];
    end
    
    if nargin == 5
        bp_band  = [1 7];
    end
    

    
    
    % Computing eog components: vertical, horizontal (and radial)
    if strcmpi(Comp, 'VHR')
        veog = s(:, 2) - mean(s(:, [1 3]), 2);
        heog = s(:, 1) - s(:, 3);
        reog = s(:, 4) - mean(s(:, 1:3), 2);
        eog_components = [veog heog reog];
    elseif strcmpi(Comp, 'VH')
        veog = s(:, 2) - mean(s(:, [1 3]), 2);
        heog = s(:, 1) - s(:, 3);
        eog_components = [veog heog];
    end
    
    NumSamples    = size(s, 1);
    NumComponents = size(eog_components, 2);

    eog_detected = false(NumSamples, 1);

    % Computing the envelop of the signal and checking for sample above
    % threshold
    for cId = 1:NumComponents
            
        c_eog   = eog_components(:, cId);
        
        c_bp_eog = filt_bp(c_eog, bp_order, bp_band, fs);
        
        env_eog = abs(hilbert(c_bp_eog));        
        win     = env_eog > Th;      
        
        eog_detected = eog_detected | win;
        
    end
    
    

end