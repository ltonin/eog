clear all;

srcpath  = '/media/Data/Research/va/20110216_b4/bp/1-7/';
dstpath  = '/media/Data/Research/va/20110216_b4/eog/';

settings.EOGChannels = {'EXG1', 'EXG2', 'EXG3', 'Oz'};
settings.EOGChannels = {'Fp2', 'Fpz', 'Fp1', 'Oz'};
settings.Triggers = [1 2];
settings.TrialPeriod = [0 3];
settings.Threshold = 30;
settings.Components = 'VHR';


[files, srcpath] = uigetfile([srcpath '*.bdf'], 'Select bdf(s) for EOG detection', 'MultiSelect', 'on');
[dstpath] = uigetdir([dstpath '/'], 'Select directory to save EOG preprocess matlab files');    

if iscell(files)
    NumFiles = length(files);
    filelist = files;
else
    NumFiles = 1;
    filelist{1} = files;
end

for fId = 1:NumFiles
    
    c_srcbdf = [srcpath filelist{fId}];
    
    disp(['[io] - Importing ' num2str(fId) '/' num2str(NumFiles) ' bdf: ' c_srcbdf])
    
    [s, h] = io_import_bdf(c_srcbdf, settings.EOGChannels, settings.Triggers);
    
    eog_detected = eog_detection(s, settings.Threshold, settings.Components);

    
    % Correlating eog detected with trials
     
    EOGIndex = false(length(h.typ), 1);
    
    for TrId = 1:length(h.typ)
        
        WinStart = h.pos(TrId) + settings.TrialPeriod(1);
        WinStop  = h.pos(TrId) + settings.TrialPeriod(2);
        c_eog = eog_detected(WinStart:WinStop, :);
        
        if sum(c_eog) > 0
            EOGIndex(TrId) = true;
        end
        
    end
    
    settings.File        = c_srcbdf;
    
    [~, name, ext] = fileparts(filelist{fId});
    [sub, identifier, date] = strread(name, '%s%s%s', 'delimiter', '_');
    process = ['eog_' settings.Components];
    
    save([dstpath '/' char(sub) '_' char(identifier) '_' process '_' char(date) '.mat'], 'EOGIndex', 'settings');
    
end