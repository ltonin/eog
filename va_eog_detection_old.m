function eogIndex = va_eog_detection(files, support)
     
    NumFiles = length(files);
    
    eogChans = support.eog.detection.channels;
    eogTh    = support.eog.detection.threshold;
    eogComp  = support.eog.detection.components;
    
    triggers = support.processing.triggers;
    TrialPeriod = support.processing.time.TrialPeriod;

    eogIndex = [];
    
    for fId = 1:NumFiles
        c_srcbdf = files{fId};
        disp(['EOG preprocessing ' c_srcbdf ': ' num2str(fId) '/' num2str(NumFiles)]);
        
        [s, h] = io_import_bdf(c_srcbdf, eogChans, triggers);
    
        eog_detected = eog_detection(s, eogTh, h.SampleRate, eogComp);
        
        % Correlating eog detected with trials    
        c_eogIndex = false(length(h.typ), 1);
    
        for TrId = 1:length(h.typ)
        
            WinStart = h.pos(TrId);
            WinStop  = h.pos(TrId) + floor(TrialPeriod*h.SampleRate);
            
            c_eog = eog_detected(WinStart:WinStop, :);
        
            if sum(c_eog) > 0
                c_eogIndex(TrId) = true;
            end
        
        end
        
        eogIndex = cat(1, eogIndex, c_eogIndex);
        
    end
end